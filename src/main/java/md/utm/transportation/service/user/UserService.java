package md.utm.transportation.service.user;

import md.utm.transportation.domain.User;
import md.utm.transportation.domain.UserCreateForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by petru on 5/28/2017.
 */
public interface UserService {

    Optional<User> getUserById(long id);

    Optional<User> getUserByEmail(String email);

    Collection<User> getAllUsers();

    User create(UserCreateForm form);
}
