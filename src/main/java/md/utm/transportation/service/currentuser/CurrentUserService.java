package md.utm.transportation.service.currentuser;

import md.utm.transportation.domain.CurrentUser;

/**
 * Created by petru on 5/28/2017.
 */
public interface CurrentUserService {

    boolean canAccessUser(CurrentUser currentUser, Long userId);

}