package md.utm.transportation.domain;

/**
 * Created by petru on 5/28/2017.
 */
public enum Role {
    USER, ADMIN
}
