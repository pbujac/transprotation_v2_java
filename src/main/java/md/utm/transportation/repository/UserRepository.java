package md.utm.transportation.repository;

/**
 * Created by petru on 5/28/2017.
 */

import md.utm.transportation.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByEmail(String email);
}