package md.utm.transportation.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by pit on 4/26/17.
 */
@Controller
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping("/")
    public String welcomeAction(Map<String, Object> model) {
        LOGGER.debug("Getting home page");
        return "home";
    }

    @RequestMapping("/add-new-roadmap")
    public String addRoadmapAction() {

        return "roadmap/roadmap";
    }

}
