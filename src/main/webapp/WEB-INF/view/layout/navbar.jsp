<div class="navbar-custom">
    <div class="container">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li class="has-submenu">
                    <a href="#"><i class="md md-dashboard"></i>Foi de parcurs</a>
                    <ul class="submenu">
                        <li>
                            <a href="/">Vizualizare tabel</a>
                        </li>
                        <li>
                            <a href="/">Evidenta medicala</a>
                        </li>
                        <li>
                            <a href="/">Adaugare nou</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#"><i class="md md-color-lens"></i>Persoane</a>
                    <ul class="submenu">
                        <li>
                            <a href="/">Vizualizare angajati</a>
                        </li>
                        <li>
                            <a href="/">Adaugare nou</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#"><i class="md md-color-lens"></i>Vehicul</a>
                    <ul class="submenu">
                        <li>
                            <a href="/">Vizualizare vehicule</a>
                        </li>
                        <li>
                            <a href="/">Adaugare nou</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- End navigation menu        -->
        </div>
    </div> <!-- end container -->
</div> <!-- end navbar-custom -->