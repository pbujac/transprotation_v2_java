<!-- ========== LEFT SIDEBAR ========== -->
<div class="left side-menu">

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">

            <!-- ========== LIST ITEMS ========== -->
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Foi de parcurs </span>
                        <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="/">Foi de parcurs</a></li>
                        <li><a href="/">Evidenta Medicala</a></li>
                    </ul>
                </li>
            </ul>

            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>