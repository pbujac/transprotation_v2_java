<#import "/spring.ftl" as spring />


<link rel="stylesheet" type="text/css" href="<@spring.url '/css/bootstrap.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/css/core.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/css/components.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/css/icons.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/css/pages.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/css/responsive.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url 'plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url 'plugins/switchery/css/switchery.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url 'plugins/multiselect/css/multi-select.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url 'plugins/select2/css/select2.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url 'plugins/bootstrap-select/css/bootstrap-select.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url 'plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'/>"/>

<link rel="stylesheet" type="text/css" href="<@spring.url '/plugins/timepicker/bootstrap-timepicker.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/plugins/clockpicker/css/bootstrap-clockpicker.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<@spring.url '/plugins/bootstrap-daterangepicker/daterangepicker.css'/>"/>



<link rel="stylesheet" type="text/css" href="<@spring.url '/css/overrides.css'/>"/>