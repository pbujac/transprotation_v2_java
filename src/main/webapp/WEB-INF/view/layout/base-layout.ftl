<#macro myLayout title="Transport">


    <#assign logoUrl>/images/favicon_1.ico</#assign>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Transport Project - Roadmap Evidence && Medical Evidence">
    <meta name="author" content="Petru Bujac">

    <link rel="shortcut icon" href="${logoUrl}">

    <title>${title}</title>

    <!-- ========== CSS ========== -->
    <#include "css-files.ftl"/>
</head>

<body class="fixed-left">
    <#nested/>
    <!- ========== JS ========== -->
    <#include "js-files.ftl"/>
</body>
</html>
</#macro>