<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<script src="js/modernizr.min.js"></script>

<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/detect.js"></script>
<script src="/js/fastclick.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.blockUI.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="/plugins/datatables/jszip.min.js"></script>
<script src="/plugins/datatables/pdfmake.min.js"></script>
<script src="/plugins/datatables/vfs_fonts.js"></script>
<script src="/plugins/datatables/buttons.html5.min.js"></script>
<script src="/plugins/datatables/buttons.print.min.js"></script>
<script src="/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="/plugins/datatables/dataTables.colVis.js"></script>
<script src="/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script src="/plugins/select2/js/select2.js"></script>
<script src="/plugins/select2/js/i18n/ro.js"></script>
<script src="/plugins/autocomplete/jquery.mockjax.js"></script>
<script src="/plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script src="/plugins/autocomplete/countries.js"></script>
<script src="/pages/autocomplete.js"></script>
<script src="/pages/datatables.init.js"></script>

<!-- App core js -->
<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>


<script src="/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
<script src="/plugins/switchery/js/switchery.min.js"></script>
<script  src="/plugins/multiselect/js/jquery.multi-select.js"></script>
<script  src="/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="/plugins/bootstrap-select/js/bootstrap-select.min.js" ></script>
<script src="/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" ></script>
<script src="/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" ></script>
<script src="/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" ></script>

<script src="/plugins/moment/moment.js"></script>
<script src="/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="/plugins/parsleyjs/parsley.min.js"></script>

<script src="/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
<script src="/plugins/autoNumeric/autoNumeric.js" ></script>

<script  src="/pages/jquery.form-advanced.init.js"></script>
<script src="/pages/jquery.form-pickers.init.js"></script>

<script >
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
        $('#datatable-scroller').DataTable({
            ajax: "/plugins/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
        var table = $('#datatable-fixed-col').DataTable({
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            }
        });

        $("#roadmapVehicles").select2({
            language: {
                noResults: function () {
                    return 'Nu au fost gasite rezultate pentru ' + event.target.value + '<br><br><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modal-vehicles">Adauga</button>'
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });

        $("#roadmapRemorca").select2({
            language: {
                noResults: function () {
                    return 'Nu au fost gasite rezultate pentru ' + event.target.value + '<br><br><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modal-remorca">Adauga</button>'
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $("#roadmapDrivers").select2({
            language: {
                noResults: function () {
                    return 'Nu au fost gasite rezultate pentru ' + event.target.value +'<br><br><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modal-drivers">Adauga</button>'
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            maximumSelectionLength: 2

        });

        $('form').parsley();
    });
    TableManageButtons.init();
    jQuery(function($) {
        $('.autonumber').autoNumeric('init');
    });

</script>
<script src="/plugins/parsleyjs/i18n/ro.js"></script>
<script src="/plugins/parsleyjs/i18n/ro.extra.js"></script>