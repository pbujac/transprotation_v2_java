<#import "base-layout.ftl" as layout>

<#assign title>Home Page</#assign>

<#macro userLayout title="${title}">



    <@layout.myLayout "${title}">
    <div id="wrapper">
        <!-- ========== HEADER ========== -->
        <#include "header.ftl"/>

        <!-- ========== MAIN CONTENT ========== -->
        <div class="content-page">

            <div class="content">
                <div class="container">
                    <#nested/>
                </div>
            </div>
            <#include "footer.ftl"/>
        </div>

    </div>

    </@layout.myLayout>
</#macro>