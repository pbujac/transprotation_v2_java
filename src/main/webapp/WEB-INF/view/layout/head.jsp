<meta http-equiv="Content-Type" content="text/html; charset=UTF-16">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-16">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Transportation Project">
<meta name="author" content="Petru Bujac">
<!-- App Favicon icon -->
<%--<link rel="shortcut icon" href="images/favicon.ico">--%>
<!-- App Title -->
<title>Transportation Project</title>
<!-- Plugins css-->
<link href="/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<link href="/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<link href="/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

<link href="/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="/plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
<link href="/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/css/core.css" rel="stylesheet" type="text/css" />
<link href="/css/components.css" rel="stylesheet" type="text/css" />
<link href="/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/css/pages.css" rel="stylesheet" type="text/css" />
<link href="/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="/css/overrides.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="/js/modernizr.min.js"></script>