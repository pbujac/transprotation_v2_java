<!-- ========== NAVBAR ========== -->
<div class="navbar navbar-default" role="navigation">

    <div class="container">

        <!-- ========== BURGER MOBILE ========== -->
        <div class="pull-left">
            <button class="button-menu-mobile open-left waves-effect waves-light">
                <i class="md md-menu"></i>
            </button>
            <span class="clearfix"></span>
        </div>


        <!-- ========== SEARCH ========== -->
        <form role="search" class="navbar-left app-search pull-left hidden-xs">
            <input type="text" placeholder="Search..." class="form-control">
            <a href=""><i class="fa fa-search"></i></a>
        </form>


        <!-- ========== LOGOUT ========== -->
        <ul class="nav navbar-nav navbar-right pull-right">
            <li>
            <#if currentUser??>
                <form action="/logout" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button type="submit"><i class="ti-power-off m-r-10 text-danger"></i> Logout</button>
                </form>
            </#if>
            </li>
        </ul>

    </div>

</div>
