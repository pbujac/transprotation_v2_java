
    <!-- ========== TOP BAR ========== -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="/" class="logo"><i class="md md-album icon-c-logo"></i><span>
                Transp<i class="md md-album"></i>rt</span></a>
            </div>
        </div>

        <!-- ========== NAVBAR ========== -->
    <#include "navbar.ftl"/>

    </div> <!-- END TOPBAR DIV -->

    <!-- ========== LEFT SIDEBAR ========== -->
<#include "left-sidebar.ftl"/>

