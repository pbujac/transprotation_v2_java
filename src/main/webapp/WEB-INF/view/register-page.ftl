<#import "layout/user-layout.ftl" as layout>
<#import "/spring.ftl" as spring>

<@layout.userLayout "Register User page">

<div class="wrapper-page" onload="myFunction()">
    <div class=" card-box">

        <h4 class="page-title">Register User</h4>

        <@spring.bind "form" />
        <#if spring.status.error>
            <#list spring.status.errorMessages as error>
                <h5 class="text-danger center-block">${error}</h5>
            </#list>
        </#if>

        <#if currentUser?? && (currentUser.role == "ADMIN" || currentUser.role == "SYS_ADMIN")>

            <div class="panel-body">
                <form role="form" name="form" class="form-horizontal m-t-20" action="" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="email" placeholder="Email" name="email" id="email"
                                   value="${form.email}" required autofocus/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" id="password" required=""
                                   placeholder="Password"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="passwordRepeated" id="passwordRepeated"
                                   required placeholder="Repeat Password"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <select name="role" id="role" required class="form-control">
                                <option selected disabled> ROLE</option>
                                <option>USER</option>
                                <#if form.role == 'SYS_ADMIN'>
                                    <option>ADMIN</option></#if>
                            </select>
                        </div>
                    </div>

                    <div class="form-group text-center m-t-40">
                        <div class="col-md-4 col-xs-12">
                            <button class="btn btn-info btn-block text-uppercase waves-effect waves-light"
                                    type="submit">
                                Register
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </#if>

    </div>
</div>
</@layout.userLayout>