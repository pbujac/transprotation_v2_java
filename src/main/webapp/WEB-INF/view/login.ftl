<#--<!DOCTYPE html>-->
<#--<html lang="en">-->
<#--<head>-->
    <#--<meta charset="utf-8">-->
    <#--<title>Log in</title>-->
<#--</head>-->
<#--<body>-->
<#--<nav role="navigation">-->
    <#--<ul>-->
        <#--<li><a href="/">Home</a></li>-->
    <#--</ul>-->
<#--</nav>-->

<#--<h1>Log in</h1>-->

<#--<p>You can use: demo@localhost / demo</p>-->

<#--<form role="form" action="/login" method="post">-->
    <#--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->

    <#--<div>-->
        <#--<label for="email">Email address</label>-->
        <#--<input type="email" name="email" id="email" required autofocus/>-->
    <#--</div>-->
    <#--<div>-->
        <#--<label for="password">Password</label>-->
        <#--<input type="password" name="password" id="password" required/>-->
    <#--</div>-->
    <#--<div>-->
        <#--<label for="remember-me">Remember me</label>-->
        <#--<input type="checkbox" name="remember-me" id="remember-me"/>-->
    <#--</div>-->
    <#--<button type="submit">Sign in</button>-->
<#--</form>-->

<#--<#if error.isPresent()>-->
<#--<p>The email or password you have entered is invalid, try again.</p>-->
<#--</#if>-->
<#--</body>-->
<#--</html>-->


<#import "layout/base-layout.ftl" as layout>

<@layout.myLayout "Login Page">
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> Sign In to <strong class="text-custom">Transport</strong> </h3>
        </div>

        <#if error.isPresent()>
            <h5  class="text-danger -align-center">
                The email or password you have entered is invalid, try again.
            </h5>
        </#if>

        <div class="panel-body">
                <form class="form-horizontal m-t-20" role="form" action="/login" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" id="email" name="email" required="" placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                    <input class="form-control" type="password" name="password" id="password" required placeholder="Password"/>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup remember-me"  name="remember-me" type="checkbox">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

                <#--<div class="form-group m-t-30 m-b-0">-->
                    <#--<div class="col-sm-12">-->
                        <#--<a href="page-recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>-->
                    <#--</div>-->
                <#--</div>-->
            </form>

        </div>
    </div>
</div>

</@layout.myLayout>