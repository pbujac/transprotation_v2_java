<#import "layout/user-layout.ftl" as layout>

<@layout.userLayout "Test page">
<div class="row">
    <div class="col-sm-12">

        <h4 class="page-title">Foi de parcurs</h4>

        <#if currentUser?? && currentUser.role == "ADMIN">
            <a href="/user-create">Create a new user</a>
            <a href="/users">View all users</a></li>
        </#if>


        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><b>Foi de Parcurs</b></h4>
            <p class="text-muted font-13 m-b-30">
                Vizualizare, Adaugare, Editare si Stergere Foi de Parcurs
            </p>
            <div style="margin-bottom: 20px" class="col-md-2">
                <a href="/add-new-roadmap"  class="btn btn-primary btn-rounded waves-effect waves-light">Adaugare nou</a>
            </div>
            <div class="col-md-12">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Salary</th>
                        <th>Salary</th>
                        <th>Salary</th>
                    </tr>
                    </thead>


                    <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td><a class="btn btn-success waves-effect waves-light" href="/">Edit</a></td>
                        <td><a class="btn btn-danger waves-effect waves-light" href="/">Delete</a></td>
                    </tr>
                    <tr>
                        <td>Garrett Winters</td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>63</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td><a class="btn btn-success waves-effect waves-light" href="/">Edit</a></td>
                        <td><a class="btn btn-danger waves-effect waves-light" href="/">Delete</a></td>
                    </tr>

                    </tbody>
                </table>
            </div>
    </div>
</div>

</@layout.userLayout>