<!DOCTYPE html>
<%--<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>--%>
<%@ page pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html >
<head>
    <%@include file='layout/head.jsp' %>
    <%--CSS FILES--%>
    <%@include file='layout/css.jsp' %>
</head>
<body>

<%@include file='layout/header.jsp' %>

<div class="wrapper">
    <div class="container">
        <div class="row">

            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title"><b>Foi de Parcurs</b></h4>
                    <p class="text-muted font-13 m-b-30">
                        Vizualizare, Adaugare, Editare si Stergere Foi de Parcurs
                    </p>
                    <div style="margin-bottom: 20px" class="col-md-2">
                        <a href="/add-new-roadmap"  class="btn btn-primary btn-rounded waves-effect waves-light">Adaugare nou</a>
                    </div>
                    <div class="col-md-12">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                                <th>Salary</th>
                                <th>Salary</th>
                            </tr>
                            </thead>


                            <tbody>
                            <tr>
                                <td>Tiger Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>61</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                                <td><a class="btn btn-success waves-effect waves-light" href="/">Edit</a></td>
                                <td><a class="btn btn-danger waves-effect waves-light" href="/">Delete</a></td>
                            </tr>
                            <tr>
                                <td>Garrett Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>63</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                                <td><a class="btn btn-success waves-effect waves-light" href="/">Edit</a></td>
                                <td><a class="btn btn-danger waves-effect waves-light" href="/">Delete</a></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<%--FOOTER--%>
<%@include file='layout/footer.jsp' %>

<%--JS FILES--%>
<%@include file='layout/js.jsp' %>

</body>
</html>