<div class="form-group">

    <div class="col-sm-6">
        <label class="col-sm-12 ">Data eliberarii</label>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="zi/luna/an"
                   id="data-eliberarii" parsley-trigger="change" required>
            <span class="input-group-addon bg-custom b-0 text-white"><i
                    class="icon-calender"></i></span>
        </div><!-- input-group -->
    </div>

    <div class="col-sm-6">
        <label class="col-sm-12 ">Ora eliberarii</label>
        <div class="input-group m-b-15 ">
            <div class="bootstrap-timepicker">
                <input id="timpul-eliberarii" type="text" class="form-control" required>
            </div>
            <span class="input-group-addon bg-custom b-0 text-white"><i
                    class="glyphicon glyphicon-time"></i></span>
        </div><!-- input-group -->
    </div>
</div>


<div class="form-group">
    <div class="col-sm-6">
        <label class=" col-sm-12">Data valabilitatii</label>
        <div class="input-group ">
            <input type="text" class="form-control" placeholder="zi/luna/an"
                   id="data-valabilitatii" parsley-trigger="change" required>
            <span class="input-group-addon bg-custom b-0 text-white"><i
                    class="icon-calender"></i></span>
        </div><!-- input-group -->
    </div>
    <div class="col-sm-6">
        <label class=" col-sm-12">Ora valabilitatii</label>
        <div class="input-group m-b-15">
            <div class="bootstrap-timepicker">
                <input id="timpul-valabilitatii" type="text" class="form-control"
                       parsley-trigger="change" required>
            </div>
            <span class="input-group-addon bg-custom b-0 text-white"><i
                    class="glyphicon glyphicon-time"></i></span>
        </div><!-- input-group -->
    </div>
</div>