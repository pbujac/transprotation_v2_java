<div id="modal-drivers" class="modal fade" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Forma de adaugare sofer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="" novalidate>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nume" class="control-label">Prenume</label>
                                <input type="text" class="form-control" id="nume" placeholder="Ion"
                                       parsley-trigger="change" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prenume" class="control-label">Nume</label>
                                <input type="text" class="form-control" id="prenume"
                                       placeholder="Vasilachi" parsley-trigger="change" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addressDriver">Adresa</label>
                                    <input type="text"
                                           class="form-control"
                                           name="addressDriver"
                                           parsley-trigger="change"
                                           required
                                           placeholder="Introduceti adresa"
                                           id="addressDriver"
                                           style=" z-index: 2; background: transparent;"/>
                                    <input type="text" name="addressDriver" id="addressDriver-x"
                                           disabled="disabled" class="form-control"
                                           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class=" col-sm-12">Ziua de nastere</label>
                                <div class="col-sm-6">
                                    <div class="input-group ">
                                        <input type="text" class="form-control" placeholder="zi/luna/an"
                                               id="ziua-de-nastere" parsley-trigger="change" required>
                                        <span class="input-group-addon bg-custom b-0 text-white"><i
                                                class="icon-calender"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" id="employeeJob"
                               placeholder="Functia detinuta" value="SOFER">


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                    Anulati
                </button>
                <button type="submit" class="btn btn-info waves-effect waves-light"
                        onclick="console.log('save driver')">Salvati
                </button>
            </div>

        </div>
    </div>
</div><!-- /.modal -->