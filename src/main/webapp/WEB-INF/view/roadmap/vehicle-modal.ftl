<div id="modal-vehicles" class="modal fade" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Forma de adaugare a vehicolului</h4>
            </div>
            <div class="modal-body">
                <form action="" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehiclePlateNumber">Numarul de inregistrare</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehiclePlateNumber"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti numarul de inregistrare"
                                       id="vehiclePlateNumber"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehiclePlateNumber-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehicleBrand">Alegeti marca vehicolului</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehicleBrand"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti marca  vehicolului"
                                       id="vehicleBrand"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehicleBrand-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehicleModel">Alegeti modelul vehicolului</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehicleModelRemorca"
                                       placeholder="Introduceti modelul vehicolului"
                                       id="vehicleModel"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehicleModel-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehicleType">Alegeti tipul vehicolului</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehicleType"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti modelul vehicolului"
                                       id="vehicleType"
                                />
                                <input type="text" name="addressDriver" id="vehicleType-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                    Anulati
                </button>
                <button type="submit" class="btn btn-info waves-effect waves-light"
                        onclick="console.log('save vehicle')">Salvati
                </button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->