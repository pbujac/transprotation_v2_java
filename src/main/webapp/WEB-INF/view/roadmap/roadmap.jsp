<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
    <%@include file='../layout/head.jsp' %>
    <%--CSS FILES--%>
    <%@include file='../layout/css.jsp' %>
</head>
<body>

<%@include file='../layout/header.jsp' %>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Foaie de parcurs</b></h4>
                    <p class="text-muted m-b-30 font-13">
                      Adaugare foaie de parcurs
                    </p>
                    <div class="row">

                        <div class="col-md-10 col-md-offset-1">
                            <form id="roadmapForm" class="form-horizontal" role="form" action="#" novalidate>
                                <div class="form-group">
                                    <label for="roadmapSeries">Seria foii de parcurs</label>
                                    <input type="text"
                                           class="form-control"
                                           name="roadmapSeries"
                                           parsley-trigger="change"
                                           required
                                           placeholder="Introduceti seria foii de parcurs"
                                           id="roadmapSeries"
                                           style=" z-index: 2; background: transparent;"/>
                                    <input type="text" name="roadmapSeries" id="roadmapSeries-x"
                                           disabled="disabled" class="form-control"
                                           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>
                                </div>

                                <div class="form-group">
                                    <label for="roadmapNumber">Numarul foii de parcurs</label>
                                    <input type="text"
                                           class="form-control"
                                           name="roadmapNumber"
                                           parsley-trigger="change"
                                           required
                                           placeholder="Introduceti seria foii de parcurs"
                                           id="roadmapNumber"
                                           style=" z-index: 2; background: transparent;"/>
                                    <input type="text" name="roadmapNumber" id="roadmapNumber-x"
                                           disabled="disabled" class="form-control"
                                           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                </div>


                               <div class="form-group">
                                   <label for="roadmapDrivers">Soferi</label>
                                   <select class="select2-limiting" data-placeholder="Selectati soferi..." multiple="multiple" id="roadmapDrivers" parsley-trigger="change" required>
                                       <optgroup label="A">
                                           <option value="1"> Antoci Vasile</option>
                                           <option value="2">Aghpot Petru</option>
                                       </optgroup>
                                       <optgroup label="B">
                                           <option value="3"> Bratu Oleg</option>
                                           <option value="4"> Berzan Andrei</option>

                                       </optgroup>
                                       <optgroup label="c">
                                           <option value="5">Cinacu Cristian</option>
                                           <option value="6">Chelu Mihai</option>
                                       </optgroup>

                                   </select>
                               </div>

                                <div class="form-group">
                                    <label for="roadmapVehicles">Alegeti masina</label>
                                    <select class="form-control select2" id="roadmapVehicles" parsley-trigger="change" required>
                                        <option value="">Selectati masina...</option>
                                        <optgroup label="A">
                                            <option value="1"> Audi X5</option>
                                            <option value="2">Audi 80</option>
                                        </optgroup>
                                        <optgroup label="B">
                                            <option value="3"> BMW x5</option>
                                            <option value="4"> Bentley</option>
                                        </optgroup>
                                    </select>
                                </div>



                                <div class="form-group">
                                    <label for="roadmapRemorca">Alegeti remorca</label>
                                    <select class="form-control select2" id="roadmapRemorca" parsley-trigger="change" required>
                                        <option value="">Selectati remorca...</option>
                                        <optgroup label="A">
                                            <option value="1"> Audi X5</option>
                                            <option value="2">Audi 80</option>
                                        </optgroup>
                                        <optgroup label="B">
                                            <option value="3"> BMW x5</option>
                                            <option value="4"> Bentley</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group">

                                    <div class="col-sm-6">
                                        <label class="col-sm-12 ">Data eliberarii</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="zi/luna/an" id="data-eliberarii" parsley-trigger="change" required>
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                        </div><!-- input-group -->
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="col-sm-12 ">Ora eliberarii</label>
                                        <div class="input-group m-b-15 ">
                                            <div class="bootstrap-timepicker">
                                                <input id="timpul-eliberarii" type="text" class="form-control" required>
                                            </div>
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class=" col-sm-12">Data valabilitatii</label>
                                        <div class="input-group ">
                                            <input type="text" class="form-control" placeholder="zi/luna/an" id="data-valabilitatii" parsley-trigger="change" required>
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                    <div class="col-sm-6">
                                        <label class=" col-sm-12">Ora valabilitatii</label>
                                        <div class="input-group m-b-15">
                                            <div class="bootstrap-timepicker">
                                                <input id="timpul-valabilitatii" type="text" class="form-control" parsley-trigger="change" required>
                                            </div>
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="roadmapSeries">Localitatea de pornire</label>
                                    <input type="text"
                                           class="form-control"
                                           name="roadmapSeries"
                                           parsley-trigger="change"
                                           required
                                           placeholder="Introduceti adresa punctului de plecare"
                                           id="roadmapDepartureLocality"
                                           style=" z-index: 2; background: transparent;"/>
                                    <input type="text" name="country" id="roadmapDepartureLocality-x"
                                           disabled="disabled" class="form-control"
                                           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>
                                </div>

                                <div class="form-group">
                                    <label for="roadmapSeries">Localitatea de sosire</label>
                                    <input type="text"
                                           class="form-control"
                                           name="roadmapSeries"
                                           parsley-trigger="change"
                                           required
                                           placeholder="Introduceti adresa punctului de sosire"
                                           id="roadmapArrivedLocality"
                                           style=" z-index: 2; background: transparent;"/>
                                    <input type="text" name="country" id="roadmapArrivedLocality-x"
                                           disabled="disabled" class="form-control"
                                           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>
                                </div>

                                <div class="form-group">
                                    <label>Cantintetea de combustibil</label>
                                    <input type="text" placeholder="Introduceti cantitatea de combustibil" class="form-control autonumber" data-a-sep="," data-a-dec="." required>
                                    <span class="font-13 text-muted">e.g. "1,890.12"</span>
                                </div>
                                <div class="form-group m-l-10">
                                    <button type="submit" class="btn btn-purple waves-effect waves-light">Salvare</button>
                                </div>
                            </form>
                        </div>


                        <div id="modal-drivers" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title">Forma de adaugare sofer</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <form action=""  novalidate>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nume" class="control-label">Prenume</label>
                                                        <input type="text" class="form-control" id="nume" placeholder="Ion" parsley-trigger="change" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="prenume" class="control-label">Nume</label>
                                                        <input type="text" class="form-control" id="prenume" placeholder="Vasilachi" parsley-trigger="change" required>
                                                    </div>
                                                </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="addressDriver">Adresa</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="addressDriver"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti adresa"
                                                               id="addressDriver"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="addressDriver-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <label class=" col-sm-12">Ziua de nastere</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group ">
                                                            <input type="text" class="form-control" placeholder="zi/luna/an" id="ziua-de-nastere" parsley-trigger="change" required>
                                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                                        </div><!-- input-group -->
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" class="form-control" id="employeeJob" placeholder="Functia detinuta" value="SOFER">


                                        </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Anulati</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light" onclick="console.log('save driver')">Salvati</button>
                                        </div>

                                </div>
                            </div>
                        </div><!-- /.modal -->

                        <div id="modal-vehicles" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title">Forma de adaugare a vehicolului</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="" novalidate>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehiclePlateNumber">Numarul de inregistrare</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehiclePlateNumber"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti numarul de inregistrare"
                                                               id="vehiclePlateNumber"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehiclePlateNumber-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehicleBrand">Alegeti marca vehicolului</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehicleBrand"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti marca  vehicolului"
                                                               id="vehicleBrand"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehicleBrand-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehicleModel">Alegeti modelul vehicolului</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehicleModelRemorca"
                                                               placeholder="Introduceti modelul vehicolului"
                                                               id="vehicleModel"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehicleModel-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehicleType">Alegeti tipul vehicolului</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehicleType"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti modelul vehicolului"
                                                               id="vehicleType"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehicleType-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Anulati</button>
                                        <button type="submit" class="btn btn-info waves-effect waves-light" onclick="console.log('save vehicle')">Salvati</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal -->

                        <div id="modal-remorca" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title">Forma de adaugare a remorcei</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="" novalidate>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehiclePlateNumberRemorca">Numarul de inregistrare</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehiclePlateNumberRemorca"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti numarul de inregistrare"
                                                               id="vehiclePlateNumberRemorca"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehiclePlateNumberRemorca-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehicleBrandRemorca">Alegeti marca vehicolului</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehicleBrandRemorca"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti marca  vehicolului"
                                                               id="vehicleBrandRemorca"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehicleBrandRemorca-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehicleModelRemorca">Alegeti modelul vehicolului</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehicleModelRemorca"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti modelul vehicolului"
                                                               id="vehicleModelRemorca"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehicleModelRemorca-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vehicleTypeRemorca">Alegeti tipul vehicolului</label>
                                                        <input type="text"
                                                               class="form-control"
                                                               name="vehicleTypeRemorca"
                                                               parsley-trigger="change"
                                                               required
                                                               placeholder="Introduceti modelul vehicolului"
                                                               id="vehicleTypeRemorca"
                                                               style=" z-index: 2; background: transparent;"/>
                                                        <input type="text" name="addressDriver" id="vehicleTypeRemorca-x"
                                                               disabled="disabled" class="form-control"
                                                               style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Anulati</button>
                                        <button type="submit" class="btn btn-info waves-effect waves-light" onclick="console.log('save vehicle')">Salvati</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal -->

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<%--FOOTER--%>
<%@include file='../layout/footer.jsp' %>

<%--JS FILES--%>
<%@include file='../layout/js.jsp' %>

</body>
</html>