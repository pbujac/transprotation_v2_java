<div class="form-group">
    <label for="roadmapVehicles">Alegeti masina</label>
    <select class="form-control select2" id="roadmapVehicles" parsley-trigger="change" required>
        <option value="">Selectati masina...</option>
        <optgroup label="A">
            <option value="1"> Audi X5</option>
            <option value="2">Audi 80</option>
        </optgroup>
        <optgroup label="B">
            <option value="3"> BMW x5</option>
            <option value="4"> Bentley</option>
        </optgroup>
    </select>
</div>


<div class="form-group">
    <label for="roadmapRemorca">Alegeti remorca</label>
    <select class="form-control select2" id="roadmapRemorca" parsley-trigger="change" required>
        <option value="">Selectati remorca...</option>
        <optgroup label="A">
            <option value="1"> Audi X5</option>
            <option value="2">Audi 80</option>
        </optgroup>
        <optgroup label="B">
            <option value="3"> BMW x5</option>
            <option value="4"> Bentley</option>
        </optgroup>
    </select>
</div>