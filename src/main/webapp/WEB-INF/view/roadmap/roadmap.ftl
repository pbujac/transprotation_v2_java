<#import "../layout/user-layout.ftl" as layout>

<@layout.userLayout "Roadmap Page">
<div class="row">
    <div class="col-sm-12">

        <div class="card-box">

            <h4 class="m-t-0 header-title"><b>Foaie de parcurs</b></h4>
            <p class="text-muted m-b-30 font-13">
                Adaugare foaie de parcurs
            </p>
            <div class="row">

                <div class="col-md-10 col-md-offset-1">
                    <form id="roadmapForm" class="form-horizontal" role="form" action="#" novalidate>
                        <div class="form-group">
                            <label for="roadmapSeries">Seria/Numarul foii de parcurs</label>


                            <!-- ========== SERIES ROADMAP ========== -->
                            <input type="text"
                                   class="form-control"
                                   name="roadmapSeries"
                                   parsley-trigger="change"
                                   required
                                   placeholder="Introduceti seria foii de parcurs"
                                   id="roadmapSeries"
                            />
                        </div>

                        <#include "add-person.ftl" >

                        <#include "add-car.ftl" >

                        <#include "add-date.ftl" >

                        <#include "add-address.ftl" >


                        <!-- ========== COMBUSTIBLE ROADMAP ========== -->
                        <div class="form-group">
                            <label>Cantintetea de combustibil</label>
                            <input type="text" placeholder="Introduceti cantitatea de combustibil"
                                   class="form-control autonumber" data-a-sep="," data-a-dec="." required>
                            <span class="font-13 text-muted">e.g. "1,890.12"</span>
                        </div>
                        <div class="form-group m-l-10">
                            <button type="submit" class="btn btn-purple waves-effect waves-light">Salvare</button>
                        </div>
                    </form>
                </div>

                <!-- ========== MODALS ========== -->
                <#include "person-modal.ftl" >

                <#include "vehicle-modal.ftl" >

                <#include "semitrailer-modal.ftl" >

            </div>
        </div>
    </div>
</div>

</@layout.userLayout>