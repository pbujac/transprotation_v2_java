<div class="form-group">
    <label for="roadmapSeries">Localitatea de pornire</label>
    <input type="text"
           class="form-control"
           name="roadmapSeries"
           parsley-trigger="change"
           required
           placeholder="Introduceti adresa punctului de plecare"
           id="roadmapDepartureLocality"
    />
    <input type="text" name="country" id="roadmapDepartureLocality-x"
           disabled="disabled" class="form-control"
           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>
</div>

<div class="form-group">
    <label for="roadmapSeries">Localitatea de sosire</label>
    <input type="text"
           class="form-control"
           name="roadmapSeries"
           parsley-trigger="change"
           required
           placeholder="Introduceti adresa punctului de sosire"
           id="roadmapArrivedLocality"
    />
    <input type="text" name="country" id="roadmapArrivedLocality-x"
           disabled="disabled" class="form-control"
           style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>
</div>