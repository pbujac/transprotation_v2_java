
<div id="modal-remorca" class="modal fade" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Forma de adaugare a remorcei</h4>
            </div>
            <div class="modal-body">
                <form action="" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehiclePlateNumberRemorca">Numarul de inregistrare</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehiclePlateNumberRemorca"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti numarul de inregistrare"
                                       id="vehiclePlateNumberRemorca"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehiclePlateNumberRemorca-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehicleBrandRemorca">Alegeti marca vehicolului</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehicleBrandRemorca"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti marca  vehicolului"
                                       id="vehicleBrandRemorca"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehicleBrandRemorca-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehicleModelRemorca">Alegeti modelul vehicolului</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehicleModelRemorca"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti modelul vehicolului"
                                       id="vehicleModelRemorca"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehicleModelRemorca-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="vehicleTypeRemorca">Alegeti tipul vehicolului</label>
                                <input type="text"
                                       class="form-control"
                                       name="vehicleTypeRemorca"
                                       parsley-trigger="change"
                                       required
                                       placeholder="Introduceti modelul vehicolului"
                                       id="vehicleTypeRemorca"
                                       style=" z-index: 2; background: transparent;"/>
                                <input type="text" name="addressDriver" id="vehicleTypeRemorca-x"
                                       disabled="disabled" class="form-control"
                                       style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>

                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                    Anulati
                </button>
                <button type="submit" class="btn btn-info waves-effect waves-light"
                        onclick="console.log('save vehicle')">Salvati
                </button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->