
<div class="form-group">
    <label for="roadmapDrivers">Soferi</label>
    <select class="select2-limiting" data-placeholder="Selectati soferi..." multiple="multiple"
            id="roadmapDrivers" parsley-trigger="change" required>
        <optgroup label="A">
            <option value="1"> Antoci Vasile</option>
            <option value="2">Aghpot Petru</option>
        </optgroup>
        <optgroup label="B">
            <option value="3"> Bratu Oleg</option>
            <option value="4"> Berzan Andrei</option>

        </optgroup>
        <optgroup label="c">
            <option value="5">Cinacu Cristian</option>
            <option value="6">Chelu Mihai</option>
        </optgroup>

    </select>
</div>